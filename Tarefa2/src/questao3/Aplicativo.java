package questao3;

import java.util.Scanner;

public class Aplicativo {
	
	static final int MAXCANDIDATOS = 5;
	static Candidato candidatos[] = new Candidato[MAXCANDIDATOS];
	static Scanner ler = new Scanner(System.in);
	
	public static void main(String[] args) {
		int opcao;
		do {
			System.out.println("**Menu Principal**");
			System.out.println("1-Cadastrar candidatos");
			System.out.println("2-Votar");
			System.out.println("3-Resultado");
			System.out.println("4-Sair");
			System.out.println("Digite sua op��o: ");
			opcao = ler.nextInt();
			switch(opcao) {
			case 1: cadastrar(); break;
			case 2: votar(); break;
			case 3: apurarResultado(); break;
			case 4: break;
			}
		}while(opcao!=6);
		
	}

	public static void cadastrar() {
		
		for (int i = 0; i < MAXCANDIDATOS; i++) {
			System.out.println("Digite o c�digo do candidato: ");
			int codigo = ler.nextInt();
			ler.nextLine();
			System.out.println("Digite o nome: ");
			String nome = ler.nextLine();
			candidatos[i] = new Candidato(codigo, nome);
		}
		
	}

	public static void votar() {
		
		System.out.println("Digite o c�digo do candidato: ");
		int codigo = ler.nextInt();
		for (int i = 0; i < MAXCANDIDATOS; i++) {
			if (codigo == candidatos[i].getNumero()) {
				candidatos[i].incrementarVotos();
				System.out.println("Voto Validado!");
			}
		}
		
	}

	public static void apurarResultado() {
		
		int total = 0;
		int maior = 0;
		for (int i = 0; i < MAXCANDIDATOS; i++) {
			total += candidatos[i].getVotos();
			if (candidatos[i].getVotos() > maior) {
				maior = candidatos[i].getVotos();
			}
		}
		
		for (int i = 0; i < MAXCANDIDATOS; i++) {
			System.out.println(candidatos[i].getNome());
			System.out.println((candidatos[i].getVotos()*100)/total + "% dos votos");
		}
		
		for (int i = 0; i < MAXCANDIDATOS; i++) {
			if (maior == candidatos[i].getVotos()) {
				System.out.println("Vencedor(a): " + candidatos[i].getNome());
			}
		}
	}

}
