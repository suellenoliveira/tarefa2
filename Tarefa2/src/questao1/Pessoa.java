package questao1;

public class Pessoa {

	private String nome;
	private int idade;
	private char sexo;	
	
	
	public Pessoa(String nome, int idade, char sexo) {
		this.nome = nome;
		this.idade = idade;
		this.sexo = sexo;
	}
	
	public String imprimir(){
		return this.nome + "		" + this.idade + "	" + this.sexo;
	}
	
}
