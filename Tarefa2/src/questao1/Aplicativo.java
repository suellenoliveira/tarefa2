package questao1;

import java.io.IOException;
import java.util.Scanner;

public class Aplicativo {
	
	static final int MAXPESSOAS = 10;
	static int index = 0;
	static Pessoa[] lista = new Pessoa[MAXPESSOAS];
	static Scanner ler = new Scanner(System.in);


	public static void main(String[] args) throws IOException {
		int opcao;
		
		do{
			System.out.println("*** MENU PRINCIPAL ***");
            System.out.println("1-Inserir pessoa");
            System.out.println("2-Listar pessoas");
            System.out.println("3-Sair");
            System.out.println("Digite sua op��o: ");
            opcao = ler.nextInt();
            switch(opcao){
            case 1: inserirPessoa(); break;
            case 2: listarPessoas(); break;
            case 3: break;
            }
		}while(opcao!=3);

	}
	
	public static void inserirPessoa() throws IOException {

		ler.nextLine();
		System.out.println("Digite o nome da pessoa:");
        String nome = ler.nextLine();
        System.out.println("Digite a idade da pessoa:");
        int idade = ler.nextInt();
        System.out.println("Digite o sexo da pessoa(f/m):");
        char sexo = (char)System.in.read();
        
        lista[index++] = new Pessoa(nome, idade, sexo);
		
	}

	public static void listarPessoas() {
		System.out.println("Nome:		Idade:	Sexo:");
		for (int i=0; i<lista.length-1;i++){
			if (lista[i] != null){
				System.out.println(lista[i].imprimir());
			}	else {
				break;
			}
	}

	}

}
