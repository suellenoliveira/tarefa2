package questao2;

import java.util.Scanner;

public class Aplicativo {

	static Banco banco;
	static Scanner ler = new Scanner(System.in);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int opcao;
		do {
		System.out.println("**MENU PRINCIPAL**");
		System.out.println("1-Cadastrar Conta");
		System.out.println("2-Excluir Conta");
		System.out.println("3-Saldo da Conta");
		System.out.println("4-Debitar");
		System.out.println("5-Creditar");
		System.out.println("6-Transferir");
		System.out.println("7-Sair");
		System.out.println("Digite sua op��o: ");
		opcao = ler.nextInt();
		switch(opcao){
		case 1: incluirConta(); break;
		case 2: excluirConta(); break;
		case 3: imprirSaldo(); break;
		case 4: debitar(); break;
		case 5: creditar(); break;
		case 6: transferir(); break;
		case 7: break;
		}
		} while (opcao!=7);

	}

	private static void incluirConta() {
		
		System.out.println("Digite o n�mero da conta: ");
		int numero = ler.nextInt();
		System.out.println("Digite o saldo da conta: ");
		double valor = ler.nextDouble();
		
		banco.cadastrarConta(new Conta(numero, valor));
	}

	private static void excluirConta() {
		
		System.out.println("Digite o n�mero da conta: ");
		int numero = ler.nextInt();
		
		banco.excluirConta(numero);
	}

	private static void imprirSaldo() {
		
		System.out.println("Digite o numero da conta: ");
		int numero = ler.nextInt();
		
		banco.imprimirSaldo(numero);
	}

	private static void debitar() {
		
		System.out.println("Digite o n�mero da conta:");
		int numero = ler.nextInt();
		System.out.println("Digite o saldo a ser debitado: ");
		double valor = ler.nextDouble();
		
		banco.debitar(numero, valor);
	}

	private static void creditar() {
		
		System.out.println("Digite o n�mero da conta: ");
		int numero = ler.nextInt();
		System.out.println("Digite o saldo a ser creditado: ");
		double valor = ler.nextDouble();
		
		banco.creditar(numero, valor);
	}

	private static void transferir() {
		// TODO Auto-generated method stub
		System.out.println("Digite o n�mero da conta(ORIGEM): ");
		int conta1 = ler.nextInt();
		System.out.println("Digite o numero da conta(DESTINO): ");
		int conta2 = ler.nextInt();
		System.out.println("Digite o valor a ser transferido: ");
		double valor = ler.nextDouble();
		
		banco.tranferir(conta1, conta2, valor);
	}

}
