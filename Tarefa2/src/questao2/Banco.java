package questao2;

public class Banco {
	
	private static int index;
	private Conta[] contas;
	private final int MAXCONTAS = 5;
	
	public Banco() {
		contas = new Conta[MAXCONTAS];
		index = 0;
	}
	
	public void cadastrarConta(Conta c) {
		if (index < MAXCONTAS) {
			contas[index] = c;
			index++;
			System.out.println("Conta cadastrada com sucesso!");
		}	else {
				System.out.println("Conta n�o cadastrada! Tente novamente mais tarde!");
		}
	}
	
	public void excluirConta(int numero) {
		
		if (index == 0) {
			System.out.println("Essa conta n�o exixte!");
		}	else {
				for (int i = 0; i < index; i++) {
					if (numero == contas[i].getNumero()) {
						contas[i] = new Conta(0, 0);
						System.out.println("Conta excluida!");
					}
				}
			}
	}
	

	
	public void imprimirSaldo(int numero) {

		if (index == 0) {
			System.out.println("Essa conta n�o existe!");
		}	else {
				for (int i = 0; i < index; i++) {
					if (numero == contas[i].getNumero()) {
						System.out.println("Saldo: " + contas[i].getSaldo());
					}
				}
		}
	}
	
	public void debitar(int numero, double valor) {
		if (index == 0) {
			System.out.println("Essa conta n�o existe!");
		}	else {
				for (int i = 0; i < index; i++) {
					if (numero == contas[i].getNumero()) {
						contas[i].debitar(valor);
						System.out.println("Saldo debitado com sucesso!");
					}
				}
		}
	}
	
	public void creditar(int numero, double valor) {
		if (index == 0) {
			System.out.println("Essa conta n�o exixte!");
		}	else {
				for (int i = 0; i < index; i++) {
					if (numero == contas[i].getNumero()) {
						contas[i].creditar(valor);
						System.out.println("Saldo creditado com sucesso!");
					}
				}
		}
	}
	
	public void tranferir(int conta1, int conta2, double valor) {
		if (index <= 1) {
			System.out.println("Uma das contas n�o existe!");
		}	else {
				for (int i = 0; i < index; i++) {
					if (conta1 == contas[i].getNumero()) {
						contas[i].debitar(valor);
					}
					if (conta2 == contas[i].getNumero()) {
						contas[i].creditar(valor);
						System.out.println("Transfer�ncia realizada com sucesso!");
					}
				}
		}
	}

}
